package com.market.api.marketAPI.domain.service;

import com.market.api.marketAPI.domain.DomainPurchase;
import com.market.api.marketAPI.domain.repository.IDomainPurchaseRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    private IDomainPurchaseRepository domainPurchaseRepository;

    public List<DomainPurchase> getAll(){
        return domainPurchaseRepository.getAll();
    }

    public Optional<List<DomainPurchase>> getByClient(String clientId){
        return domainPurchaseRepository.getByClient(clientId);
    }

    public DomainPurchase save(DomainPurchase purchase){
        return domainPurchaseRepository.save(purchase);
    }
}

