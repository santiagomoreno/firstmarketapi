package com.market.api.marketAPI.domain.repository;

import com.market.api.marketAPI.domain.DomainPurchase;

import java.util.List;
import java.util.Optional;

public interface IDomainPurchaseRepository {

    List<DomainPurchase> getAll();
    Optional<List<DomainPurchase>> getByClient(String clientId);
    DomainPurchase save(DomainPurchase purchase);
}
