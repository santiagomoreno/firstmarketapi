package com.market.api.marketAPI.persistence.mapper;

import com.market.api.marketAPI.domain.DomainProduct;
import com.market.api.marketAPI.persistence.entity.Product;
import org.mapstruct.*;

import java.util.List;


@Mapper(componentModel = "spring", uses = {CategoryMapper.class})
public interface ProductMapper {

    @Mappings({
            @Mapping(source = "idProduct", target = "productId"),
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "idCategory", target = "categoryId"),
            @Mapping(source = "price", target = "price"),
            @Mapping(source = "stock", target = "stock"),
            @Mapping(source = "state", target = "active"),
            @Mapping(source = "category", target = "category")

    })
    DomainProduct toDomainProduct(Product product);

    List<DomainProduct> toDomainProducts(List<Product> product);

    @InheritInverseConfiguration
    @Mapping(target = "barcode", ignore = true)
    Product toProduct(DomainProduct product);
}
