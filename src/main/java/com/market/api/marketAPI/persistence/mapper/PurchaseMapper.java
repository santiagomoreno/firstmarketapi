package com.market.api.marketAPI.persistence.mapper;

import com.market.api.marketAPI.domain.DomainPurchase;
import com.market.api.marketAPI.persistence.entity.Purchase;
import org.mapstruct.*;


import java.util.List;

@Mapper(componentModel = "spring", uses = {PurchaseItemMapper.class})
public interface PurchaseMapper {

    @Mappings({
            @Mapping(source = "idPurchase", target = "purchaseId"),
            @Mapping(source = "idClient", target = "clientId"),
            @Mapping(source = "purchaseProduct", target = "items")
    })
    DomainPurchase toDomainPurchase(Purchase product);
    List<DomainPurchase> toDomainPurchases(List<Purchase> purchases);

    @InheritInverseConfiguration
    @Mapping(target = "client", ignore = true)
    Purchase toPurchase(DomainPurchase product);
}
