package com.market.api.marketAPI.persistence;

import com.market.api.marketAPI.domain.DomainProduct;
import com.market.api.marketAPI.domain.repository.IDomainProductRepository;
import com.market.api.marketAPI.persistence.crud.IProductRepository;
import com.market.api.marketAPI.persistence.entity.Product;
import com.market.api.marketAPI.persistence.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository implements IDomainProductRepository {

    @Autowired
    private IProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<DomainProduct> getAll(){
        List<Product> products = (List<Product>) productRepository.findAll();

        return productMapper.toDomainProducts(products);
    }

    @Override
    public Optional<List<DomainProduct>> getByCategory(int categoryId) {
        List<Product> products = (List<Product>) productRepository.findByIdCategoryOrderByNameAsc(categoryId);

        return Optional.of(productMapper.toDomainProducts(products));
    }

    @Override
    public Optional<List<DomainProduct>> getScarseProducts(int quantity) {
        Optional<List<Product>> products = productRepository.findByStockLessThanAndState(quantity,true);
        return products.map(prods -> productMapper.toDomainProducts(prods));
    }

    public Optional<DomainProduct> getProduct(int idProduct){
        return productRepository.findById(idProduct).map( product -> productMapper.toDomainProduct(product));
    }

    @Override
    public DomainProduct save(DomainProduct domainProduct) {
        Product product = productMapper.toProduct(domainProduct);
        return productMapper.toDomainProduct(productRepository.save(product));
    }

    public void delete(int idProduct){
        productRepository.deleteById(idProduct);
    }
}