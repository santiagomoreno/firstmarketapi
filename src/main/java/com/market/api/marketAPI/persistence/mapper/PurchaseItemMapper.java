package com.market.api.marketAPI.persistence.mapper;

import com.market.api.marketAPI.domain.DomainPurchaseItem;
import com.market.api.marketAPI.persistence.entity.PurchaseProduct;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {ProductMapper.class})

public interface PurchaseItemMapper {

    @Mappings({
            @Mapping(source = "id.idProduct", target = "productId"),
            @Mapping(source = "state", target = "active")
    })
    DomainPurchaseItem toPurchaseItem(PurchaseProduct product);

    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "id.idPurchase", ignore = true),
            @Mapping(target = "purchase", ignore = true),
            @Mapping(target = "product", ignore = true)
    })
    PurchaseProduct toPurchaseProduct(DomainPurchaseItem product);
}