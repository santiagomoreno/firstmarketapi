package com.market.api.marketAPI.persistence.crud;

import com.market.api.marketAPI.persistence.entity.Client;
import org.springframework.data.repository.CrudRepository;

public interface IClientRepository extends CrudRepository<Client, Integer> {
}

