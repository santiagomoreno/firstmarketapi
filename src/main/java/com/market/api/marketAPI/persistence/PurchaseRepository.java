package com.market.api.marketAPI.persistence;

import com.market.api.marketAPI.domain.DomainPurchase;
import com.market.api.marketAPI.domain.repository.IDomainPurchaseRepository;
import com.market.api.marketAPI.persistence.crud.IPurchaseRepository;
import com.market.api.marketAPI.persistence.entity.Purchase;
import com.market.api.marketAPI.persistence.mapper.PurchaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository implements IDomainPurchaseRepository {

    @Autowired
    private IPurchaseRepository purchaseRepository;

    @Autowired
    private PurchaseMapper purchaseMapper;

    @Override
    public List<DomainPurchase> getAll() {

        List<Purchase> purchases = (List<Purchase>) purchaseRepository.findAll();

        return purchaseMapper.toDomainPurchases(purchases);
    }

    @Override
    public Optional<List<DomainPurchase>> getByClient(String clientId) {

        return purchaseRepository.findByidClient(clientId)
                .map(purchase -> purchaseMapper.toDomainPurchases(purchase));
    }

    @Override
    public DomainPurchase save(DomainPurchase purchase) {
        Purchase newPurchase = purchaseMapper.toPurchase(purchase);
        newPurchase.getPurchaseProduct().forEach(product -> product.setPurchase(newPurchase));

        return purchaseMapper.toDomainPurchase(purchaseRepository.save(newPurchase));
    }
}