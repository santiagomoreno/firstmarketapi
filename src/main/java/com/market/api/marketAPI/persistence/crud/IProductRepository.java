package com.market.api.marketAPI.persistence.crud;

import com.market.api.marketAPI.persistence.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IProductRepository extends CrudRepository<Product, Integer> {

    List<Product> findByIdCategory(int IdCategory);

    List<Product> findByIdCategoryOrderByNameAsc(int IdCategory);

    Optional<List<Product>> findByStockLessThanAndState(int stock, boolean state);
}
