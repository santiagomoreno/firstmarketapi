package com.market.api.marketAPI.persistence.crud;

import com.market.api.marketAPI.persistence.entity.Purchase;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IPurchaseRepository extends CrudRepository<Purchase, Integer> {

    Optional<List<Purchase>> findByidClient(String idClient);

}
